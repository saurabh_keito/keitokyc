"""Helper File
@author: Aditya Aman, Abhishek Inamdar
Copyright (C) 2019 Keito Tech Private Ltd. - All Rights Reserved
This File contains helper methods such as sending json encoded response etc.
"""
import zipfile
import time
import os
import json
import psutil
import shutil


def send_json_response(status, msg="", data=None):
    to_send = {
        "status": status,
        "message": msg,
        "data": data
    }
    return json.dumps(to_send, default=str)


def get_dirs_and_files(source_dir):
    file_list = []
    for file_nm in os.listdir(source_dir):
        dir_nm = os.path.join(source_dir, file_nm)
        if os.path.isfile(dir_nm):  # checking whether it is file
            file_list.append(dir_nm)
        elif os.path.isdir(dir_nm):  # checking whether it is directory
            file_list.extend(get_dirs_and_files(dir_nm))  # recursive call
    return file_list


def make_zip(file_list, dest_dir, source_dir, zip_nm):
    for file_ in file_list:
        dest_dir.write(file_, os.path.relpath(file_, source_dir))
    return os.path.abspath(zip_nm)


def make_zip_from_folder(input_dir, output_dir):
    # getting list of directories and files
    file_list = get_dirs_and_files(input_dir)
    zip_nm = os.path.join(output_dir, str(time.time()))
    # creating zip file
    zip_file = zipfile.ZipFile(zip_nm, 'w', zipfile.ZIP_DEFLATED, allowZip64=True)
    abs_path = make_zip(file_list, zip_file, input_dir, zip_nm)
    zip_file.close()
    return abs_path


def is_valid_file(file, extension):
    ext = file.filename.split(".")[-1].lower()
    if ext == extension:
        return True
    return False


def is_cpu_free(wait=0):
    """
    * Recursive function which checks if CPU is available to take up
    * new task. If wait is over 30 seconds then return False.
    :param wait: Time the script has been waiting
    :return: Boolean
    """
    if wait > 30:
        return False
    cpu_percent = psutil.cpu_percent()
    if cpu_percent > 20:
        wait += 5
        time.sleep(5)
        return is_cpu_free(wait)
    else:
        return True


def save_file(file, save_path):
    if not os.path.exists(save_path):
        os.mkdir(save_path)
    file.save(os.path.join(save_path, file.filename))


def get_file_type(filename):
    return filename.split(".")[-1].lower()


def move_to_path(path, filename, file_type, rel_path):
    folder_path = os.path.join(rel_path, file_type)
    if not os.path.exists(folder_path):
        os.mkdir(folder_path)
    move_to = os.path.join(folder_path, filename)
    shutil.move(path, move_to)


def debug_exception(sysinfo):
    err = {"errtype": str(sysinfo[0]), "err": str(sysinfo[1].args),
           "fileName": sysinfo[2].tb_frame.f_globals['__file__'],
           "funcName": sysinfo[2].tb_frame.f_code.co_name, "errLine": sysinfo[2].tb_lineno}
    print(err)
