WEBSITE = "http://localhost:5000/"

BASE_PATH = "/home/user/PycharmProjects/keito_kyc/KeitoKYC/"

gpu = '0'

HED_PROTO_PATH=f"{BASE_PATH}app/KYC/cropping/cv_dnn/deploy.prototxt"

HED_MODEL_PATH=f"{BASE_PATH}app/KYC/cropping/cv_dnn/hed_pretrained_bsds.caffemodel"

FACE_PROTO_PATH=f"{BASE_PATH}app/KYC/cropping/face/deploy.prototxt.txt"

FACE_MODEL_PATH=f"{BASE_PATH}app/KYC/cropping/face//res10_300x300_ssd_iter_140000.caffemodel"

text_detection_checkpoint_path = f"{BASE_PATH}app/KYC/ocr/tex_detection/checkpoints_mlt"
