"""
* @author: Aditya Aman
* Copyright (C) 2019 Keito Tech Private Ltd. - All Rights Reserved
"""
import enum


class Messages(enum.Enum):

    bad_request = "Bad request, please check your inputs"
    unknown_class = "This type of forms is not yet been added to the server, please contact admin."
    server_error = "Sorry! the server encountered an error, it can be the request parameters was missing."
    file_not_found = "The file was not found on the server, maybe it was deleted or corrupted."
