GOOGLE_CLOUD_PROJECT="keito-uat"
export GOOGLE_CLOUD_PROJECT

GOOGLE_APPLICATION_CREDENTIALS="/home/user/projects/image-labelling/bucket.json"
export GOOGLE_APPLICATION_CREDENTIALS

BUCKET_ID="form-labels"
export BUCKET_ID

USE_SSL="no"
export USE_SSL

FLASK_APP="app"
export FLASK_APP

FLASK_ENV="development"
export FLASK_ENV

DB_URI="localhost:3306"
export DB_URI

DB_NAME="labels"
export DB_NAME

DB_USER="user"
export DB_USER

DB_PASS="admin@123"
export DB_PASS

CA_PATH="/home/ca.pem"
export CA_PATH

CLIENT_CERT="/home/server-cert.pem"
export CLIENT_CERT

CLIENT_KEY="/home/server-key.pem"
export CLIENT_KEY

HED_PROTO_PATH="/home/user/Documents/image-labelling/py/cv_dnn/deploy.prototxt"
export HED_PROTO_PATH

HED_MODEL_PATH="/home/user/Documents/image-labelling/py/cv_dnn/hed_pretrained_bsds.caffemodel"
export HED_MODEL_PATH

FACE_PROTO_PATH="/home/user/Documents/image-labelling/py/face/deploy.prototxt.txt"
export FACE_PROTO_PATH

FACE_MODEL_PATH="/home/user/Documents/image-labelling/py/face/res10_300x300_ssd_iter_140000.caffemodel"
export FACE_MODEL_PATH
