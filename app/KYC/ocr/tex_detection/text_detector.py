# coding=utf-8
import logging
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '5'  # FATAL
logging.getLogger('tensorflow').setLevel(logging.FATAL)
import sys

import cv2
import numpy as np
import tensorflow as tf

from KeitoKYC.app.utilities import CONSTANTS

sys.path.append(os.getcwd())
from ..tex_detection.nets import model_train as model
from .utils.rpn_msr.proposal_layer import proposal_layer
from .utils.text_connector.detectors import TextDetector


def resize_image(img):
    img_size = img.shape
    im_size_min = np.min(img_size[0:2])
    im_size_max = np.max(img_size[0:2])

    im_scale = 1 #float(600) / float(im_size_min)
    if np.round(im_scale * im_size_max) > 1200:
        im_scale = float(1200) / float(im_size_max)
    new_h = int(img_size[0] * im_scale)
    new_w = int(img_size[1] * im_scale)

    new_h = new_h if new_h // 16 == 0 else (new_h // 16 + 1) * 16
    new_w = new_w if new_w // 16 == 0 else (new_w // 16 + 1) * 16

    re_im = cv2.resize(img, (new_w, new_h), interpolation=cv2.INTER_LINEAR)
    return re_im, (new_h / img_size[0], new_w / img_size[1])



with tf.get_default_graph().as_default():
    input_image = tf.placeholder(tf.float32, shape=[None, None, None, 3], name='input_image')
    input_im_info = tf.placeholder(tf.float32, shape=[None, 3], name='input_im_info')

    global_step = tf.get_variable('global_step', [], initializer=tf.constant_initializer(0), trainable=False)

    bbox_pred, cls_pred, cls_prob = model.model(input_image)

    variable_averages = tf.train.ExponentialMovingAverage(0.997, global_step)
    saver = tf.train.Saver(variable_averages.variables_to_restore())

sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
ckpt_state = tf.train.get_checkpoint_state(CONSTANTS.text_detection_checkpoint_path)
model_path = os.path.join(CONSTANTS.text_detection_checkpoint_path, os.path.basename(ckpt_state.model_checkpoint_path))
# print('Restore from {}'.format(model_path))
saver.restore(sess, model_path)
os.environ['CUDA_VISIBLE_DEVICES'] = CONSTANTS.gpu

def get_corners(boxes,img_shape):
    # print(boxes)
    final_boxes = []
    for box in boxes:
        xmin = min(box[0], box[2], box[4], box[6])
        xmax = max(box[0], box[2], box[4], box[6])
        ymin = min(box[1], box[3], box[5], box[7])
        ymax = max(box[1], box[3], box[5], box[7])
        w, h = xmax-xmin, ymax-ymin

        xmin = max(int(xmin - ((w / 100) * 5)), 0)
        ymin = max(int(ymin - ((h / 100) * 25)), 0)
        xmax = min(int(xmax + ((w / 100) * 1)), img_shape[1])
        ymax = min(int(ymax + ((h / 100) * 1)), img_shape[0])


        final_boxes.append([xmin, ymin, xmax, ymax])
        final_boxes = np.array(final_boxes)
        sorted_indx = np.lexsort((final_boxes[:,0], final_boxes[:,1]))
        final_boxes = [list(b) for b in final_boxes[sorted_indx]]

    return final_boxes

def get_text_boxes(img):

    img, (rh, rw) = resize_image(img)
    h, w, c = img.shape
    im_info = np.array([h, w, c]).reshape([1, 3])
    bbox_pred_val, cls_prob_val = sess.run([bbox_pred, cls_prob],
                                           feed_dict={input_image: [img],
                                                      input_im_info: im_info})

    textsegs, _ = proposal_layer(cls_prob_val, bbox_pred_val, im_info)
    scores = textsegs[:, 0]
    textsegs = textsegs[:, 1:5]

    textdetector = TextDetector(DETECT_MODE='H')
    boxes = textdetector.detect(textsegs, scores[:, np.newaxis], img.shape[:2])
    boxes = np.array(boxes, dtype=np.int)
    boxes = get_corners(boxes, img.shape[:2])
    return boxes, scores


if __name__ == '__main__':
    tf.app.run()
