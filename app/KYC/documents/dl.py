import os
import multiprocessing
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
from ..kyc.kyc import *
from ..ocr.tex_detection import text_detector
from PIL import Image
import pytesseract

class Dl(KeitoKYC):

    def __init__(self,img_path):
        super().__init__(img_path)
        self.texts = []

    def crop_and_rotate_document(self, img=None):
        '''

        :param img:  document image to be cropped and rotate
        :return: cropped then rotated image (if applicable)
        '''

        if type(img) == type(None):
            img = self.img_color
        data = get_biggest_face(img)
        if data:
            xmin, ymin, xmax, ymax = data
            im = cv2.rectangle(img.copy(), (xmin, ymin), (xmax, ymax), (0, 255, 0), 1)

            h, w = ymax - ymin, xmax - xmin

            face_to_image_ratio = img.shape[0] / h
            print("face_to_image_ratio", face_to_image_ratio)
            if face_to_image_ratio > 7:
                if xmin < img.shape[1] // 2:
                    print("left")
                    xmin = max(int(xmin - ((w / 100) * 100)), 0)
                    ymin = max(int(ymin - ((h / 100) * 100)), 0)
                    xmax = min(int(xmax + ((w / 100) * 480)), img.shape[1])
                    ymax = min(int(ymax + ((h / 100) * 150)), img.shape[0])

                else:
                    print("right")
                    xmin = max(int(xmin - ((w / 100) * 450)), 0)
                    ymin = max(int(ymin - ((h / 100) * 100)), 0)
                    xmax = min(int(xmax + ((w / 100) * 150)), img.shape[1])
                    ymax = min(int(ymax + ((h / 100) * 170)), img.shape[0])

                cropped = img.copy()[ymin:ymax, xmin:xmax]
                boxes_all, boxes = get_word_boxes(cropped)
                avg_deg = get_avg_word_angle(boxes_all)
                ret, rotated_img = rotate_image(img, avg_deg)

                rotated_img = rotated_img[ymin:ymax, xmin:xmax]
                # text = pytesseract.image_to_string(Image.fromarray(rotated_img))
                # print(text)
            else:
                boxes_all, boxes = get_word_boxes(img)
                avg_deg = get_avg_word_angle(boxes_all)
                ret, rotated_img = rotate_image(img, avg_deg)

        else:
            boxes_all, boxes = get_word_boxes(img)
            avg_deg = get_avg_word_angle(boxes_all)
            ret, rotated_img = rotate_image(img, avg_deg)
        return ret, rotated_img


    def thread(self,i):
        text_box_img = self.img_color.copy()[self.boxes[i][1]:self.boxes[i][3], self.boxes[i][0]:self.boxes[i][2]]
        im_pil = Image.fromarray(text_box_img)
        text = pytesseract.image_to_string(im_pil, config="--psm 6")
        # text_dict = pytesseract.image_to_data(im_pil, config="--psm 6", output_type=pytesseract.Output.DICT)
        print(text)
        self.texts.append([text,[self.boxes[i][0], self.boxes[i][1], self.boxes[i][2], self.boxes[i][3]]])
        cv2.imshow("text_box",text_box_img)
        cv2.waitKey(0)

    def process_document(self, processes=int(multiprocessing.cpu_count() / 2)):
        print("processing document")
        img = self.img_color.copy()
        ret, rotated_img = self.crop_and_rotate_document(img)

        boxes, scores = text_detector.get_text_boxes(rotated_img)
        cv2.imshow("rotated_img", rotated_img)
        self.img_color = rotated_img
        self.boxes = boxes
        # for i, box in enumerate(boxes):
        #     cv2.polylines(rotated_img, [box[:8].astype(np.int32).reshape((-1, 1, 2))], True, color=(0, 255, 0),
        #                   thickness=2)
        pp = []

        img_box = rotated_img.copy()
        for i,b in enumerate(boxes):
            self.thread(i)
            img_box = cv2.rectangle(img_box, (boxes[i][0], boxes[i][1]),
                                    (boxes[i][2], boxes[i][3]), color=(0, 0, 255), thickness=1)
            # cv2.putText(img_box, str(boxes[i].box_id), (boxes[i].centroid_x, boxes[i].centroid_y),
            #             cv2.FONT_HERSHEY_PLAIN, 1, 255, 1)
        cv2.imwrite("boxes.png", img_box)
        extract_data(self.texts)
        # pool = multiprocessing.Pool(processes=1)
        # pool.map(self.thread, pp)
        # pool.close()
        # pool.join()
        #


        cv2.waitKey(0)
        return [], [], []

