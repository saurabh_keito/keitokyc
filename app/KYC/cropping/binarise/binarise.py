"""
* @author: Rajendraprasad Lawate
* Copyright (C) 2019 Keito Tech Private Ltd. - All Rights Reserved
"""
from __future__ import print_function
import os
os.environ['BINARISE_IP'] = "http://localhost:9000/v1/models/binarise-model:predict"
import cv2
import warnings
import numpy as np
import json
from . import util, utilDataGenerator
from keras import backend as K
import requests


def binarise_image(img):
    """
    This function returns binarised image
    :param img: gray image
    :return: binary image
    """
    util.init()
    warnings.filterwarnings('ignore')
    K.set_image_data_format('channels_last')
    step = -1
    window = 64
    threshold = 0
    if step == -1:
        step = window
    img = np.asarray(img)
    rows = img.shape[0]
    cols = img.shape[1]
    if img.shape[0] < window or img.shape[1] < window:  # Scale approach
        new_rows = window if img.shape[0] < window else img.shape[0]
        new_cols = window if img.shape[1] < window else img.shape[1]
        img = cv2.resize(img, (new_cols, new_rows), interpolation=cv2.INTER_CUBIC)
    img = np.asarray(img).astype('float32')
    img = 255. - img
    final_img = img.copy()
    for (x, y, windows) in utilDataGenerator.sliding_window(img, stepSize=step, windowSize=(window, window)):
        if windows.shape[0] != window or windows.shape[1] != window:
            continue

        roi = img[y:(y + window), x:(x + window)].copy()
        roi = roi.reshape(window, window, 1)
        roi = roi.astype('float32')
        i = {'input': roi.tolist()}
        payload = {
            "instances": [i]
        }
        r = requests.post(os.environ['BINARISE_IP'], json=payload)
        if r.status_code == 200:
            prediction = np.array(json.loads(r.content)['predictions'][0])
        else:
            raise ValueError("Binarise model response-->", r.status_code)
        prediction = (prediction > threshold)
        final_img[y:(y + window), x:(x + window)] = prediction.reshape(window, window)

    final_img = 1. - final_img
    final_img *= 255.
    final_img = final_img.astype('uint8')

    if final_img.shape[0] != rows or final_img.shape[1] != cols:
        final_img = cv2.resize(final_img, (cols, rows), interpolation=cv2.INTER_CUBIC)
    return final_img
