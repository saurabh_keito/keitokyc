import cv2
import numpy as np
import cv2
import math

def get_horizontal_kernel(kernel_length):
    return cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_length, 1))


def get_vertical_kernel(kernel_length):
    return cv2.getStructuringElement(cv2.MORPH_RECT, (1, kernel_length))


def get_kerneled_image(img, kernel, iterations, vert_dilate=None):
    img_temp2 = cv2.erode(img, kernel, iterations=iterations)
    if vert_dilate:
        iterations = vert_dilate
    return cv2.dilate(img_temp2, kernel, iterations=iterations)


def sort_contours(cnts, method="left-to-right"):
    """

    :param cnts: list of contours
    :param method: method of sorting
    :return: sorted contours and bounding boxes
    """
    # initialize the reverse flag and sort index
    reverse = False
    i = 0

    # handle if we need to sort in reverse
    if method == "right-to-left" or method == "bottom-to-top":
        reverse = True

    # handle if we are sorting against the y-coordinate rather than
    # the x-coordinate of the bounding box
    if method == "top-to-bottom" or method == "bottom-to-top":
        i = 1

    # construct the list of bounding boxes and sort them from top to
    # bottom
    bounding_boxes = [cv2.boundingRect(c) for c in cnts]
    (cnts, bounding_boxes) = zip(*sorted(zip(cnts, bounding_boxes), key=lambda b: b[1][i], reverse=reverse))

    # return the list of sorted contours and bounding boxes
    return cnts, bounding_boxes

class Box:

    def __init__(self, xmin, ymin, xmax, ymax, box_id=None, area=None):
        self.box_id = box_id
        self.value = None
        self.xmin = xmin
        self.ymin = ymin
        self.xmax = xmax
        self.ymax = ymax
        self.centroid_x = int(xmin + ((xmax - xmin) / 2))
        self.centroid_y = int(ymin + ((ymax - ymin) / 2))
        if area:
            self.area = area
        else:
            self.area = (xmax - xmin) * (ymax - ymin)

    def __str__(self):
        data = 'Box, {}, {}, {}, {}, {}'.format(self.xmin, self.ymin, self.xmax, self.ymax, self.centroid_x,
                                                self.centroid_y, self.box_id)
        return data

    def get_type(self):
        return 1, self.xmin, self.ymin, self.xmax, self.ymax, self.box_id, self.value



    @staticmethod
    def get_boxes(img, vert_flg=False, horz_flg=False, kernel_length=None, join_lines=False, iterations=2, skip_area_ratio=0.7, join_contour_count=6, **kwargs):
        """
        :param img: binary image
        :param vert_flg: for joining small breaks in vertical lines
        :param horz_flg: for joining small breaks in horizontal lines
        :param kernel_length: the kernel length acc to which the border's minimum length will be decided
        :param join_lines: joins 2 consecutive horizontal line
        if angle between there endpoints is around 90 degrees if True
        :param iterations: iterations of erosion and dilation
        :return: dict of detected box objects
        """

        if "vert_dilate" in kwargs:
            vert_dilate = kwargs['vert_dilate']
        else:
            vert_dilate = None

        img_final_bin = Box.detect_lines(
            img.copy(), vert_flg, horz_flg,
            kernel_length, join_lines=join_lines, iterations=iterations, vert_dilate=vert_dilate,
            join_contour_count=join_contour_count
        )
        height, width = img.shape
        contours, hierarchy = cv2.findContours(img_final_bin, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        if contours:
            (contours, boundingBoxes) = sort_contours(contours, method="top-to-bottom")
        else:
            return [], []
        boxes = []
        temp_boxes = {}
        idx = 0
        total_area = height * width
        for c in contours:
            if (cv2.contourArea(c) / total_area) > 0.0002:
                x, y, w, h = cv2.boundingRect(c)
                new_area = w * h
                area_ratio = new_area / total_area
                if area_ratio < skip_area_ratio:
                    temp_boxes[idx] = Box(x, y, x + w, y + h, idx, new_area)
                    boxes.append([x, y, x + w, y + h])
                    idx += 1
        return temp_boxes, boxes

    @staticmethod
    def get_horizontal_lines(img, kernel_length=None, iterations=2):
        """
        this functions returns the horizontal lines
        :param img: input binary image
        :param kernel_length: the kernel length acc to which the border's minimum length will be decided
        :param iterations: iterations of erosion and dilation
        :return: image convolved with kernel
        """
        if not kernel_length:
            kernel_length = np.array(img).shape[1] // 80
        kernel = get_horizontal_kernel(kernel_length)
        return get_kerneled_image(img, kernel, iterations)

    @staticmethod
    def get_vertical_lines(img, kernel_length=None, iterations=2, vert_dilate=None):
        """
        this functions returns the vertical lines
        :param img: input binary image
        :param kernel_length: the kernel length acc to which the border's minimum length will be decided
        :param iterations: iterations of erosion and dilation
        :return:
        """
        kernel = get_vertical_kernel(kernel_length)
        return get_kerneled_image(img, kernel, iterations, vert_dilate)

    @staticmethod
    def detect_lines(img, vert_flg=False, horz_flg=False, kernel_length=None, join_lines=False, iterations=2,
                     vert_dilate=None, join_contour_count=6):

        # Invert image
        img = 255 - img
        vertical_lines_img = Box.get_vertical_lines(img, kernel_length, iterations, vert_dilate)
        horizontal_lines_img = Box.get_horizontal_lines(img, kernel_length, iterations)

        if vert_flg:
            vertical_lines_img = Box.get_hough_lines(vertical_lines_img)
        if horz_flg:
            horizontal_lines_img = Box.get_hough_lines(horizontal_lines_img)
        #if join_lines:
         #   horizontal_lines_img = join_consecutive_lines(horizontal_lines_img, join_contour_count)

        img_final_bin = cv2.bitwise_or(vertical_lines_img, horizontal_lines_img)
        return img_final_bin

    @staticmethod
    def get_hough_lines(img):
        kernel_length = np.array(img).shape[1] // 80
        lines = cv2.HoughLinesP(image=img, rho=0.05, theta=np.pi / 600, threshold=10, lines=np.array([]),
                                minLineLength=kernel_length//4, maxLineGap=kernel_length*2)

        if not isinstance(lines, type(None)):
            a, b, c = lines.shape
            for i in range(a):
                cv2.line(img, (lines[i][0][0], lines[i][0][1]), (lines[i][0][2], lines[i][0][3]), 255, thickness=1)
        return img

