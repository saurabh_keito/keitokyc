import re
import imutils
import math
from ..cropping.word_boxes.word_boxes_serving import *
from ..cropping.binarise.binarise import *
from ..cropping.face.detect_faces import *
# from app.models import Query, generate_session
import pandas as pd


class CropLayer(object):
    def __init__(self, params, blobs):
        self.xstart = 0
        self.xend = 0
        self.ystart = 0
        self.yend = 0

    def getMemoryShapes(self, inputs):
        inputShape, targetShape = inputs[0], inputs[1]
        batchSize, numChannels = inputShape[0], inputShape[1]
        height, width = targetShape[2], targetShape[3]

        self.ystart = (inputShape[2] - targetShape[2]) // 2
        self.xstart = (inputShape[3] - targetShape[3]) // 2
        self.yend = self.ystart + height
        self.xend = self.xstart + width

        return [[batchSize, numChannels, height, width]]

    def forward(self, inputs):
        return [inputs[0][:,:,self.ystart:self.yend,self.xstart:self.xend]]


cv2.dnn_registerLayer('Crop', CropLayer)
# Load the model.
net = cv2.dnn.readNet(CONSTANTS.HED_PROTO_PATH, CONSTANTS.HED_MODEL_PATH)


def get_boxes(img):
    boxes_all, boxes = get_word_boxes(img)

    return boxes_all, boxes


def get_avg_word_angle(boxes_all):
    slopes_degrees = []
    for i in boxes_all:
        x1, y1 = i[0], i[1]
        x2, y2 = i[2], i[3]
        slopes_degrees.append(math.degrees(math.atan((y2 - y1) / (x2 - x1 + 0.000001))))


    avg_deg = sum(slopes_degrees) / (len(slopes_degrees)+0.000001)

    return avg_deg


def rotate_image(img, avg_deg):
    if avg_deg > 2 or avg_deg < -2:
        rotated = imutils.rotate(img, avg_deg+(avg_deg/10))
        return True, rotated
    else:
        return False, img


def get_edges(rotated_img):
    inp = cv2.dnn.blobFromImage(rotated_img, scalefactor=0.1,
                                swapRB=False, crop=False, mean=(104.00698793, 116.66876762, 122.67891434))
    net.setInput(inp)
    out = net.forward()
    out = out[0, 0]
    out = cv2.resize(out, (rotated_img.shape[1], rotated_img.shape[0]))
    out = 255 * out
    out = out.astype(np.uint8)
    ret, out = cv2.threshold(out, 110, 255, cv2.THRESH_BINARY_INV)

    return out


def get_binarised_image(img):
    return binarise_image(img)

def get_biggest_face(img):
    return get_face(img)


def merge_boxes_nms(boxes):
    boxes = np.array(boxes)
    boxes = non_max_suppression_fast(boxes.astype(np.float64), overlapThresh=0.2)
    return boxes


def extract_data(texts):
    date_reg = r"\d\d\/\d\d\/\d{2,4}|\v\d{2}-\d{2}-\d{2,4}\v"
    lic_no_reg = r"[A-Za-z0-9]*-[A-Za-z0-9]*-[A-Za-z0-9]*-[A-Za-z0-9]*-[A-Za-z0-9]*|[A-Za-z0-9]*-[A-Za-z0-9]*-[A-Za-z0-9]*|[a-zA-Z]\d{7,8}|\d{9,13}|\d*\s\d{3}\s\d{3}|\d{8,13}"
    height_reg = r"\d*'-\d*''|\d-\d{2}|\d*FT\d*IN|\d*-\d*''"
    weight_reg = r"\d{3}[\slbs|lb|l]*"
    name_reg = r"[A-Z][A-Za-z]*"
    add_reg = r""
    sex_reg = r"^[FM]\b|\s[FM]\b"
    # dates = []
    # lic_no = []
    # unknown = []
    not_name = ["DRIVER", "LICENSE", "BRO", "BLU", "BLK", "EYES", "EYE",
                "GENDER", "Weight", "Height", "NONE", "HGT", "WGT"
                                                             "HAIR", "BLN", "Class", "CLASS", "Hair", "SEX", "Sex",
                "RSTR", "ISS"
                        "END", "ISSUE", "EXP", "Exp", "iss", "EXF", "OPERATOR"]
    keys = []
    values = []
    coord = []
    for xx, [xmin, ymin, xmax, ymax] in texts:

        validated = xx
        d = re.findall(date_reg, validated)
        l = re.findall(lic_no_reg, validated)
        h = re.findall(height_reg, validated)
        w = re.findall(weight_reg, validated)
        n = re.findall(name_reg, validated)
        a = re.findall(add_reg, validated)
        s = re.findall(sex_reg, validated)

        if d:
            for dd in d:
                keys.append("Date")
                values.append(dd)
                coord.append([xmin, ymin, xmax, ymax])

        elif l:
            keys.append("License number")
            values.append(l[0])
            coord.append([xmin, ymin, xmax, ymax])

        elif h:
            keys.append("Height")
            values.append(h[0])
            coord.append([xmin, ymin, xmax, ymax])

        elif w:
            keys.append("Weight")
            values.append(w[0])
            coord.append([xmin, ymin, xmax, ymax])

        elif s:
            keys.append("Sex")
            values.append(s[0])
            coord.append([xmin, ymin, xmax, ymax])

        elif n:
            if n[0] not in not_name:
                keys.append("Name")
                values.append(n[0])
                coord.append([xmin, ymin, xmax, ymax])
            else:
                keys.append("Unclassified")
                values.append(validated)
                coord.append([xmin, ymin, xmax, ymax])

        else:
            keys.append("Unclassified")
            values.append(validated)
            coord.append([xmin, ymin, xmax, ymax])

    dict1 = {"Keys": keys,
             "Values": values}
    df = pd.DataFrame(data=dict1)
    # print(df)
    dict1["Coord"] = coord
    print(df)
    # df.to_excel('dict1.xlsx')

    return keys, values, coord

def merge_boxes(boxes, img=None):
    seed_box = boxes[0]




